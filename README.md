# VT100 Terminal Color in Go (golang)

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/skilstak/code/go/color)](https://goreportcard.com/report/gitlab.com/skilstak/code/go/color) [![Coverage](https://gocover.io/_badge/gitlab.com/skilstak/code/go/color)](https://gocover.io/gitlab.com/skilstak/code/go/color) [![GoDoc](https://godoc.org/gitlab.com/skilstak/code/go/color?status.svg)](https://godoc.org/gitlab.com/skilstak/code/go/color)

Embrace true color in your Go terminal applications.

## Usage

Depending on how many colors you are using and how configurable they are you could use one of the following approaches:

1. Simply copy the color constants you want into your own code.
1. Import the package an use the constants in your strings.
1. Import the package and lookup the color values from the Index.

## FAQ

### "What terminal support colors?"

All modern terminals support 256^3 colors --- including the defaults for Linux, Mac, and Windows (as of 2019) and Putty, remote connection tool.

